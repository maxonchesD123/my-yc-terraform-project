resource "yandex_compute_instance" "vm" {
  count = var.cluster_size
  name  = "ubuntu-vm-${count.index+1}"
  zone  = element(var.zones, count.index)

  resources {
    cores  = var.instance_cores
    memory = var.instance_memory
  }

  boot_disk {
    initialize_params {
      image_id = "fd8k6joqhuk8ts8eb1ao"
    }
  }

  network_interface {
    subnet_id = element(yandex_vpc_subnet.subnet.*.id, count.index)
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file(var.public_key_path)}"
  }
}

locals {
  external_ips = ["${yandex_compute_instance.vm.*.network_interface.0.nat_ip_address}"]
  internal_ips = ["${yandex_compute_instance.vm.*.network_interface.0.ip_address}"]
}
